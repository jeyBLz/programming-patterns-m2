package edu.ssau.common.model.impl;

import edu.ssau.common.exception.DuplicateModelNameException;
import edu.ssau.common.exception.ModelPriceOutOfBoundsException;
import edu.ssau.common.exception.NoSuchModelNameException;
import edu.ssau.common.model.Vehicle;
import edu.ssau.task2.Command;
import edu.ssau.task8.Visitor;

import java.io.*;
import java.util.*;
import java.util.stream.Stream;

public class Car implements Vehicle, Iterable<Car.Model> {

    private String name;
    private Model[] models;

    private transient Command command;
    private transient final Memento memento;

    public void setCommand(Command command) {
        this.command = command;
    }

    public Car(String name, int size) {
        this.name = name;
        models = new Model[size];
        memento = new Memento();

        Random random = new Random();
        Stream.iterate(0, i -> i + 1)
                .limit(size)
                .forEach(i ->
                        models[i] = new Model("Car #" + (i + 1), (random.nextInt(5000) + 5000) / 100.)
                );
    }

    @Override
    public Iterator<Model> iterator() {
        return new CarIterator();
    }

    private class CarIterator implements Iterator<Model> {

        private int index = 0;

        @Override
        public boolean hasNext() {
            return index < models.length;
        }

        @Override
        public Model next() {
            if (hasNext()) {
                return models[index++];
            }
            throw new NoSuchElementException();
        }
    }

    private class Memento {
        private byte[] car;

        public Car getCar() throws IOException, ClassNotFoundException {
            try (ByteArrayInputStream bais = new ByteArrayInputStream(car)) {
                ObjectInputStream ois = new ObjectInputStream(bais);

                return (Car) ois.readObject();
            }
        }

        public void setCar(Car car) throws IOException {
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(car);
                this.car = baos.toByteArray();
            }
        }
    }

    public void createMemento() throws IOException {
        memento.setCar(this);
    }

    public void setMemento() throws IOException, ClassNotFoundException {
        Car car = memento.getCar();
        this.name = car.name;
        this.models = car.models;
    }

    public static class Model implements Cloneable, Serializable {
        private String name;
        private double price;

        public Model(String name, double price) {
            this.name = name;
            this.price = price;
        }

        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }

        private double getPrice() {
            return price;
        }

        private void setPrice(double price) {
            this.price = price;
        }

        @Override
        public String toString() {
            return "{%s:%s}".formatted(name, price);
        }

        @Override
        protected Model clone() throws CloneNotSupportedException {
            return (Model) super.clone();
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public double getPriceByModelName(String modelName) throws NoSuchModelNameException {
        return getModelByName(modelName).getPrice();
    }

    @Override
    public void setPriceByModelName(String modelName, double price) throws NoSuchModelNameException {
        if (price < 0.) {
            throw new ModelPriceOutOfBoundsException();
        }
        getModelByName(modelName).setPrice(price);
    }

    @Override
    public String[] getModelNames() {
        return Arrays.stream(models)
                .map(Model::getName)
                .toArray(String[]::new);
    }

    @Override
    public double[] getPrices() {
        return Arrays.stream(models)
                .mapToDouble(Model::getPrice)
                .toArray();
    }

    @Override
    public void add(String modelName, double price) throws DuplicateModelNameException {
        if (price < 0.) {
            throw new ModelPriceOutOfBoundsException();
        }
        if (findModelByName(modelName).isPresent()) {
            throw new DuplicateModelNameException(String.format("Model with name %s already exists", modelName));
        }
        Model model = new Model(modelName, price);
        models = Arrays.copyOf(models, models.length + 1);
        models[models.length - 1] = model;
    }

    @Override
    public void remove(String modelName) throws NoSuchModelNameException {
        int index = Stream.iterate(0, i -> i + 1)
                .limit(models.length)
                .filter(i -> models[i].getName().equals(modelName))
                .findFirst()
                .orElseThrow(() -> new NoSuchModelNameException("There is no model with name " + modelName));
        System.arraycopy(models, index + 1, models, index, models.length - index - 1);
        models = Arrays.copyOf(models, models.length - 1);
    }

    @Override
    public int getSize() {
        return models.length;
    }

    private Optional<Model> findModelByName(String modelName) {
        return Arrays.stream(models)
                .filter(m -> m.getName().equals(modelName))
                .findFirst();
    }

    private Model getModelByName(String modelName) throws NoSuchModelNameException {
        return findModelByName(modelName)
                .orElseThrow(() -> new NoSuchModelNameException("There is no model with name " + modelName));
    }

    @Override
    public Car clone() throws CloneNotSupportedException {
        Car clone = (Car) super.clone();
        clone.models = Arrays.stream(models).map(model -> {
            try {
                return model.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return null;
        }).toArray(Model[]::new);
        return clone;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void print(OutputStream os) throws IOException {
        command.print(this, os);
    }
}
