package edu.ssau.common.model.impl;

import edu.ssau.common.exception.DuplicateModelNameException;
import edu.ssau.common.exception.NoSuchModelNameException;
import edu.ssau.common.model.Vehicle;
import edu.ssau.task8.Visitor;

import java.util.concurrent.locks.Lock;

public class SynchronizedVehicle implements Vehicle {

    private final Vehicle vehicle;
    private final Lock lock;

    public SynchronizedVehicle(Vehicle vehicle, Lock lock) {
        this.vehicle = vehicle;
        this.lock = lock;
    }

    @Override
    public String getName() {
        lock.lock();
        try {
            return vehicle.getName();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void setName(String name) {
        lock.lock();
        try {
            vehicle.setName(name);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public double getPriceByModelName(String modelName) throws NoSuchModelNameException {
        lock.lock();
        try {
            return vehicle.getPriceByModelName(modelName);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void setPriceByModelName(String modelName, double price) throws NoSuchModelNameException {
        lock.lock();
        try {
            vehicle.setPriceByModelName(modelName, price);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public String[] getModelNames() {
        lock.lock();
        try {
            return vehicle.getModelNames();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public double[] getPrices() {
        lock.lock();
        try {
            return vehicle.getPrices();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void add(String modelName, double price) throws DuplicateModelNameException {
        lock.lock();
        try {
            vehicle.add(modelName, price);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void remove(String modelName) throws NoSuchModelNameException {
        lock.lock();
        try {
            vehicle.remove(modelName);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public int getSize() {
        lock.lock();
        try {
            return vehicle.getSize();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Vehicle clone() throws CloneNotSupportedException {
        return vehicle.clone();
    }

    @Override
    public void accept(Visitor visitor) {
        lock.lock();
        try {
            vehicle.accept(visitor);
        } finally {
            lock.unlock();
        }
    }
}
