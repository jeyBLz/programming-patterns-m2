package edu.ssau.common.factory;

import edu.ssau.common.model.Vehicle;
import edu.ssau.common.model.impl.Car;

public class CarFactory implements VehicleFactory {
    @Override
    public Vehicle createInstance(String name, int size) {
        return new Car(name, size);
    }
}
