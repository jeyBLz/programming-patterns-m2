package edu.ssau.common.factory;

import edu.ssau.common.model.Vehicle;
import edu.ssau.common.model.impl.Motorcycle;

public class MotorcycleFactory implements VehicleFactory {
    @Override
    public Vehicle createInstance(String name, int size) {
        return new Motorcycle(name, size);
    }
}
