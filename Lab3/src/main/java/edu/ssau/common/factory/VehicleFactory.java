package edu.ssau.common.factory;

import edu.ssau.common.model.Vehicle;

public interface VehicleFactory {

    Vehicle createInstance(String name, int size);

}
