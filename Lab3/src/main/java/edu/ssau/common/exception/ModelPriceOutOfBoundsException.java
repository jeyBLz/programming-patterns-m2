package edu.ssau.common.exception;

public class ModelPriceOutOfBoundsException extends RuntimeException {

    public ModelPriceOutOfBoundsException() {}

    public ModelPriceOutOfBoundsException(String message) {
        super(message);
    }

}
