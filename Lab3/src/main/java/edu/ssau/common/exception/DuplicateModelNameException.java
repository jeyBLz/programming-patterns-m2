package edu.ssau.common.exception;

public class DuplicateModelNameException extends Exception {

    public DuplicateModelNameException() {}

    public DuplicateModelNameException(String message) {
        super(message);
    }

}
