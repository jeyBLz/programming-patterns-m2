package edu.ssau.task5.state;

import edu.ssau.task5.state.impl.SemesterState;

import java.awt.*;

public interface State {

    Image handle();

}
