package edu.ssau.task5;

import edu.ssau.task5.state.State;
import edu.ssau.task5.state.impl.SemesterState;
import edu.ssau.task5.state.impl.SessionState;
import edu.ssau.task5.state.impl.VacationState;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {

    private State state;
    private JButton semesterButton;
    private JButton sessionButton;
    private JButton vacationButton;

    private final int WIDTH = 1400;
    private final int HEIGHT = 700;

    public Panel() {
        state = new SemesterState();

        semesterButton = new JButton("Семестр");
        semesterButton.addActionListener((l) -> {
            state = new SemesterState();
            this.repaint();
        });

        sessionButton = new JButton("Сессия");
        sessionButton.addActionListener((l) -> {
            state = new SessionState();
            this.repaint();
        });

        vacationButton = new JButton("Каникулы");
        vacationButton.addActionListener((l) -> {
            state = new VacationState();
            this.repaint();
        });

        this.add(semesterButton);
        this.add(sessionButton);
        this.add(vacationButton);

        this.setPreferredSize(new Dimension(WIDTH, HEIGHT + 100));
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(state.handle(), 0, 100, WIDTH, HEIGHT, null);
    }
}
