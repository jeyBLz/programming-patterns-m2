package edu.ssau.task2;

import edu.ssau.common.model.Vehicle;

import java.io.IOException;
import java.io.OutputStream;

public interface Command {

    void print(Vehicle vehicle, OutputStream os) throws IOException;

}
