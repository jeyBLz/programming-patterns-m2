package edu.ssau.task2;

import edu.ssau.common.model.Vehicle;

import java.io.IOException;
import java.io.OutputStream;

public class RowCommand implements Command {

    @Override
    public void print(Vehicle vehicle, OutputStream os) throws IOException {
        String[] modelNames = vehicle.getModelNames();
        double[] modelPrices = vehicle.getPrices();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < vehicle.getSize(); i++) {
            sb.append("{").append(modelNames[i]).append(":").append(modelPrices[i]).append("}");
            if (i < vehicle.getSize() - 1) {
                sb.append(",");
            }
        }
        os.write(sb.toString().getBytes());
    }

}
