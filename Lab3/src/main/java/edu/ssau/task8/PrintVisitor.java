package edu.ssau.task8;

import edu.ssau.common.model.impl.Car;
import edu.ssau.common.model.impl.Motorcycle;

public class PrintVisitor implements Visitor {

    @Override
    public void visit(Car car) {
        for (Car.Model model : car) {
            System.out.print(model);
        }
    }

    @Override
    public void visit(Motorcycle motorcycle) {
        for (Motorcycle.Model model : motorcycle) {
            System.out.println(model);
        }
    }

}
