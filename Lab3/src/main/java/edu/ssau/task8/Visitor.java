package edu.ssau.task8;

import edu.ssau.common.model.impl.Car;
import edu.ssau.common.model.impl.Motorcycle;

public interface Visitor {

    void visit(Car car);

    void visit(Motorcycle motorcycle);

}
