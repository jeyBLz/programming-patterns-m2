package edu.ssau.task6.strategy.impl;

import edu.ssau.task6.strategy.CountStrategy;

import java.util.HashMap;
import java.util.Map;

public class SecondCountStrategy implements CountStrategy {

    @Override
    public Map<Integer, Long> count(int[] arr) {
        Map<Integer, Long> result = new HashMap<>();
        for (int el : arr) {
            result.put(el, result.getOrDefault(el, 0L) + 1);
        }
        return result;
    }

}
