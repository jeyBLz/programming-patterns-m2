package edu.ssau.task6.strategy.impl;

import edu.ssau.task6.strategy.CountStrategy;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FirstCountStrategy implements CountStrategy {

    @Override
    public Map<Integer, Long> count(int[] arr) {
        return Arrays.stream(arr).boxed().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

}
