package edu.ssau.task6.strategy;

import java.util.Map;

public interface CountStrategy {

    Map<Integer, Long> count(int[] arr);

}
