package edu.ssau.task6;

import edu.ssau.task6.strategy.impl.SecondCountStrategy;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(args[0]))) {
            oos.writeObject(new int[] {1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 1, 2});
        }

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(args[0]))) {
            int[] arr = (int[]) ois.readObject();
            System.out.println(CountUtils.count(arr));
            CountUtils.setStrategy(new SecondCountStrategy());
            System.out.println(CountUtils.count(arr));
        }
    }

}
