package edu.ssau.task6;

import edu.ssau.task6.strategy.CountStrategy;
import edu.ssau.task6.strategy.impl.FirstCountStrategy;

import java.util.Map;

public class CountUtils {

    private static CountStrategy strategy = new FirstCountStrategy();

    public static void setStrategy(CountStrategy strategy) {
        CountUtils.strategy = strategy;
    }

    public static Map<Integer, Long> count(int[] arr) {
        return strategy.count(arr);
    }
}
