package edu.ssau.task7;

import javax.swing.*;

public class SquareJumpingComponent extends AbstractJumpingComponent {

    private static final String PATH = "src/main/resources/square.png";

    public SquareJumpingComponent(JPanel panel) {
        super(new ImageIcon(PATH).getImage(), panel);
    }

}
