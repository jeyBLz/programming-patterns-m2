package edu.ssau.task7;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Panel extends JPanel {

    private final int WIDTH = 500;
    private final int HEIGHT = 500;

    private final ExecutorService executor = Executors.newFixedThreadPool(10);
    private final List<AbstractJumpingComponent> components = new LinkedList<>();

    public Panel() {
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));

        final JButton startBtn = new JButton("Start");
        startBtn.addActionListener(this::startBtnEvent);
        this.add(startBtn);

        final JButton exitBtn = new JButton("Shutdown");
        exitBtn.addActionListener(this::exitBtnEvent);
        this.add(exitBtn);
    }

    private void exitBtnEvent(ActionEvent e) {
        executor.shutdown();
        System.exit(0);
    }

    private void startBtnEvent(ActionEvent e) {
        AbstractJumpingComponent newComponent = new BallJumpingComponent(this);
        components.add(newComponent);
        executor.submit(newComponent);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        components.forEach(component -> component.paint(g));
    }
}
