package edu.ssau.task7;

import javax.swing.*;

public class BallJumpingComponent extends AbstractJumpingComponent {

    private static final String PATH = "src/main/resources/ball.png";

    public BallJumpingComponent(JPanel panel) {
        super(new ImageIcon(PATH).getImage(), panel);
    }

}
