package edu.ssau.task7;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class AbstractJumpingComponent extends JComponent implements Runnable {

    private final Image image;
    private final JPanel panel;
    private final int size;
    private int x;
    private int y;
    private int xVelocity = -10;
    private int yVelocity = -50;

    public AbstractJumpingComponent(Image image, JPanel panel) {
        this.image = image;
        this.panel = panel;
        this.size = 100;
        this.x = panel.getWidth() - size;
        this.y = panel.getHeight() - size;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(image, x, y, size, size, null);
    }

    @Override
    public void run() {
        new Timer(25, this::actionPerformed).start();
    }

    private void actionPerformed(ActionEvent e) {
        x += xVelocity;
        y += yVelocity;
        if (x <= 0 || x + size >= panel.getWidth()) {
            xVelocity *= -1;
        }
        if (y <= 0 || y + size >= panel.getHeight()) {
            yVelocity *= -1;
        }
        panel.repaint();
    }

}
