package edu.ssau.task7;

import javax.swing.*;

public class StarJumpingComponent extends AbstractJumpingComponent {

    private static final String PATH = "src/main/resources/star.png";

    public StarJumpingComponent(JPanel panel) {
        super(new ImageIcon(PATH).getImage(), panel);
    }

}
