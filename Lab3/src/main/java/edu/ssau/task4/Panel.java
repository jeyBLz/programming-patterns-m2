package edu.ssau.task4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

public class Panel extends JPanel implements MouseListener {

    private static final int WIDTH = 500;
    private static final int HEIGHT = 500;

    private final List<ObserverComponent> components;

    public Panel() {
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        this.addMouseListener(this);

        components = new ArrayList<>(3);
        components.add(new Eye(190, 200, 50));
        components.add(new Eye(260, 200, 50));
        components.add(new Nose(235, 270, 30));
        components.add(new Mouth(200, 330, 100));
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        components.forEach(component -> {
            component.paint(g);
        });
        g2.drawOval(150, 150, 200, 225);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        components.forEach(component -> {
            component.update(e.getX(), e.getY());
        });
        this.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}
}
