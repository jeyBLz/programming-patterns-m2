package edu.ssau.task4;

import java.awt.*;
import java.util.function.BiPredicate;

public class Eye implements ObserverComponent {

    private final BiPredicate<Integer, Integer> conditionOnEyePoked;
    private final int x;
    private final int y;
    private final int diameter;
    private State state;

    private enum State {
        EMPTY,
        OPEN,
        CLOSE
        ;
    }

    public Eye(int x, int y, int diameter) {
        this.x = x;
        this.y = y;
        this.diameter = diameter;
        this.state = State.OPEN;
        this.conditionOnEyePoked = (mX, mY) ->
                Math.pow(x + diameter / 2. - mX, 2) + Math.pow(y + diameter / 2. - mY, 2) < Math.pow(diameter / 2., 2);
    }

    @Override
    public void update(int x, int y) {
        if (!conditionOnEyePoked.test(x, y)) {
            return;
        }
        state = State.values()[3 - state.ordinal()];
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(5));
        if (state == State.OPEN) {
            g2.setPaint(Color.WHITE);
            g2.fillOval(x, y, diameter, diameter);
            g2.setPaint(Color.BLACK);
            g2.drawOval(x, y, diameter, diameter);
            int halfRadius = Math.floorDiv(diameter, 4);
            g2.fillOval(x + halfRadius, y + halfRadius * 2, halfRadius * 2, halfRadius * 2);
        } else {
            g2.setPaint(Color.BLACK);
            g2.drawLine(x, y + Math.floorDiv(diameter, 2), x + diameter, y + Math.floorDiv(diameter, 2));
        }
    }
}
