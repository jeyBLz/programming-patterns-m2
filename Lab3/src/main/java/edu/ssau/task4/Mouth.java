package edu.ssau.task4;

import java.awt.*;
import java.util.function.BiPredicate;

public class Mouth implements ObserverComponent {

    private final BiPredicate<Integer, Integer> conditionOnMouthPoked;

    private final int x;
    private final int y;
    private final int width;

    private final int height = 20;

    private State state;

    public Mouth(int x, int y, int width) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.state = State.CALMED;
        this.conditionOnMouthPoked = (mX, mY) ->
                mX <= x + width && mX >= x && mY <= y + height && mY >= y - height;
    }

    private enum State {
        EMPTY,
        CALMED,
        SMILING
        ;
    }

    @Override
    public void update(int x, int y) {
        if (!conditionOnMouthPoked.test(x, y)) {
            return;
        }
        this.state = State.values()[3 - this.state.ordinal()];
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(5));
        g2.setPaint(Color.BLACK);
        if (this.state == State.CALMED) {
            g2.drawLine(x, y, x + width, y);
        } else {
            g2.drawArc(x, y - height, width, height, 0, -180);
        }
    }

}
