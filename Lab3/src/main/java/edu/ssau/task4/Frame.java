package edu.ssau.task4;

import javax.swing.*;

public class Frame extends JFrame {

    private final JPanel panel;

    public Frame() {
        panel = new Panel();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.add(panel);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setResizable(false);
    }

}
