package edu.ssau.task4;

import java.awt.*;
import java.util.function.BiPredicate;

public class Nose implements ObserverComponent {

    private final BiPredicate<Integer, Integer> conditionOnNosePoked;

    private final int x;
    private final int y;
    private final int diameter;

    private NoseColor color;

    public Nose(int x, int y, int diameter) {
        this.x = x;
        this.y = y;
        this.diameter = diameter;
        this.color = NoseColor.RED;
        this.conditionOnNosePoked = (mX, mY) ->
                Math.pow(x + diameter / 2. - mX, 2) + Math.pow(y + diameter / 2. - mY, 2) < Math.pow(diameter / 2., 2);
    }

    private enum NoseColor {
        RED(Color.RED),
        ORANGE(Color.ORANGE),
        YELLOW(Color.YELLOW),
        GREEN(Color.GREEN),
        BLUE(Color.BLUE),
        PURPLE(Color.MAGENTA)
        ;

        private final Color color;

        NoseColor(Color color) {
            this.color = color;
        }
    }

    @Override
    public void update(int x, int y) {
        if (!conditionOnNosePoked.test(x, y)) {
            return;
        }
        this.color = NoseColor.values()[(this.color.ordinal() + 1) % NoseColor.values().length];
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(5));

        g2.setPaint(this.color.color);
        g2.fillOval(x, y, diameter, diameter);
        g2.setPaint(Color.BLACK);
        g2.drawOval(x, y, diameter, diameter);
    }

}
