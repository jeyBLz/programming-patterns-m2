package edu.ssau.task4;

import java.awt.*;

public interface ObserverComponent {

    void update(int x, int y);

    void paint(Graphics g);

}
