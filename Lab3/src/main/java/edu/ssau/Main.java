package edu.ssau;

import edu.ssau.common.exception.DuplicateModelNameException;
import edu.ssau.common.model.Vehicle;
import edu.ssau.common.model.impl.Car;
import edu.ssau.common.model.impl.Motorcycle;
import edu.ssau.task1.WriterUtils;
import edu.ssau.task2.ColumnCommand;
import edu.ssau.task2.RowCommand;
import edu.ssau.task8.PrintVisitor;
import edu.ssau.task8.Visitor;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, DuplicateModelNameException, ClassNotFoundException {
        System.out.println("\n-----------//CHAIN OF RESPONSIBILITY//-----------");

        Vehicle vehicle = new Car("Mercedes", 4);
        WriterUtils.write(vehicle);

        System.out.println("\n-----------//Command//-----------");

        Car car = new Car("Mercedes", 10);
        car.setCommand(new RowCommand());
        car.print(System.out);
        car.setCommand(new ColumnCommand());
        car.print(System.out);


        System.out.println("\n\n-----------//ITERATOR//-----------");

        System.out.println();

        for (Car.Model model : car) {
            System.out.println(model);
        }

        System.out.println("\n-----------//MEMENTO//-----------");

        car.createMemento();
        car.setName("Aston Martin");
        car.add("Car #11", 1);
        System.out.println(car.getName());
        for (Car.Model model : car) {
            System.out.println(model);
        }
        car.setMemento();
        System.out.println(car.getName());
        for (Car.Model model : car) {
            System.out.println(model);
        }

        System.out.println("\n-----------//VISITOR//-----------");

        Vehicle motorcycle = new Motorcycle("Yamaha", 10);
        Visitor visitor = new PrintVisitor();
        car.accept(visitor);
        motorcycle.accept(visitor);

    }

}
