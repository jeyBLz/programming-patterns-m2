package edu.ssau.task1;

import edu.ssau.common.model.Vehicle;

import java.io.IOException;
import java.util.function.Predicate;

public abstract class Writer {

    private Writer next;
    protected Predicate<Integer> condition;

    public void setNext(Writer next) {
        this.next = next;
    }

    public void apply(Vehicle vehicle) throws IOException {
        if (condition.test(vehicle.getSize())) {
            write(vehicle);
            return;
        }
        if (next != null) {
            next.apply(vehicle);
        }
    }

    public abstract void write(Vehicle vehicle) throws IOException;

}
