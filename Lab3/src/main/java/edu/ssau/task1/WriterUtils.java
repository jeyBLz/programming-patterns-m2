package edu.ssau.task1;

import edu.ssau.common.model.Vehicle;

import java.io.IOException;

public final class WriterUtils {

    private static final Writer chain;

    private WriterUtils() {}

    static {
        chain = new RowWriter((v) -> v <= 3);
        Writer second = new ColumnWriter((v) -> v > 3);

        chain.setNext(second);
    }

    public static void write(Vehicle vehicle) throws IOException {
        chain.apply(vehicle);
    }

}
