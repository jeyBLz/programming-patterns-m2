package edu.ssau.task1;

import edu.ssau.common.model.Vehicle;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.function.Predicate;

public class RowWriter extends Writer {

    public RowWriter(Predicate<Integer> condition) {
        this.condition = condition;
    }

    @Override
    public void write(Vehicle vehicle) throws IOException {
        String[] modelNames = vehicle.getModelNames();
        double[] modelPrices = vehicle.getPrices();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < vehicle.getSize(); i++) {
            sb.append("{").append(modelNames[i]).append(":").append(modelPrices[i]).append("}");
            if (i < vehicle.getSize() - 1) {
                sb.append(",");
            }
        }
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("out.txt"))) {
            bw.write(sb.toString());
        }
    }
}
