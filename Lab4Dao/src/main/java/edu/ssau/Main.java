package edu.ssau;

import edu.ssau.dao.MotorcycleDAO;
import edu.ssau.dao.impl.MotorcycleByteDAO;
import edu.ssau.dao.impl.MotorcycleTextDAO;
import edu.ssau.model.impl.Motorcycle;

public class Main {

    public static void main(String[] args) {
        MotorcycleDAO daoText = new MotorcycleTextDAO("test-text.txt");
        Motorcycle motorcycle1 = new Motorcycle("Honda", 5);
        daoText.write(motorcycle1);
        Motorcycle test1 = daoText.read();
        System.out.println(test1);

        System.out.println();

        MotorcycleDAO daoByte = new MotorcycleByteDAO("test-byte.txt");
        Motorcycle motorcycle2 = new Motorcycle("Kawasaki", 2);
        daoByte.write(motorcycle2);
        Motorcycle test2 = daoByte.read();
        System.out.println(test2);
    }

}
