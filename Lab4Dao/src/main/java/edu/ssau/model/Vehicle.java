package edu.ssau.model;

import edu.ssau.exception.DuplicateModelNameException;
import edu.ssau.exception.NoSuchModelNameException;

import java.io.Serializable;

public interface Vehicle extends Cloneable, Serializable {

    String getName();
    void setName(String name);
    double getPriceByModelName(String modelName) throws NoSuchModelNameException;
    void setPriceByModelName(String modelName, double price) throws NoSuchModelNameException;
    String[] getModelNames();
    double[] getPrices();
    void add(String modelName, double price) throws DuplicateModelNameException;
    void remove(String modelName) throws NoSuchModelNameException;
    int getSize();
    Vehicle clone() throws CloneNotSupportedException;

}
