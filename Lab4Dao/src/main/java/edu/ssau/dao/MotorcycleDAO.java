package edu.ssau.dao;

import edu.ssau.model.impl.Motorcycle;

import java.io.FileNotFoundException;

public interface MotorcycleDAO {

    String PATH = "src\\main\\resources";

    Motorcycle read();

    void write(Motorcycle motorcycle);

}
