package edu.ssau.dao.impl;

import edu.ssau.dao.MotorcycleDAO;
import edu.ssau.model.impl.Motorcycle;

import java.io.*;

public class MotorcycleByteDAO implements MotorcycleDAO {

    private String filename;

    public MotorcycleByteDAO(String filename) {
        this.filename = filename;
    }

    @Override
    public Motorcycle read() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("%s\\%s".formatted(PATH, filename)))) {
            return (Motorcycle) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void write(Motorcycle motorcycle) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("%s\\%s".formatted(PATH, filename)))) {
            oos.writeObject(motorcycle);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
