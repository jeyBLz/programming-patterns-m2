package edu.ssau.dao.impl;

import edu.ssau.dao.MotorcycleDAO;
import edu.ssau.model.impl.Motorcycle;

import java.io.*;

public class MotorcycleTextDAO implements MotorcycleDAO {

    private String filename;

    public MotorcycleTextDAO(String filename) {
        this.filename = filename;
    }

    @Override
    public Motorcycle read() {
        try (BufferedReader br = new BufferedReader(new FileReader("%s\\%s".formatted(PATH, filename)))) {
            String name = br.readLine();
            int size = Integer.parseInt(br.readLine());
            Motorcycle motorcycle = new Motorcycle(name);
            for (int i = 0; i < size; i++) {
                String[] line = br.readLine().split(", ");
                motorcycle.addWithNoExceptions(line[0], Double.parseDouble(line[1]));
            }
            return motorcycle;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void write(Motorcycle motorcycle) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("%s\\%s".formatted(PATH, filename)))) {
            bw.write(motorcycle.toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

}
