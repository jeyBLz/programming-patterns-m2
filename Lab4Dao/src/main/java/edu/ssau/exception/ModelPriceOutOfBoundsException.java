package edu.ssau.exception;

public class ModelPriceOutOfBoundsException extends RuntimeException {

    public ModelPriceOutOfBoundsException() {}

    public ModelPriceOutOfBoundsException(String message) {
        super(message);
    }

}
