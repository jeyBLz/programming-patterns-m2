package edu.tinkoff.repository;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.async.annotation.SingleResult;
import io.micronaut.data.repository.reactive.ReactiveStreamsCrudRepository;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;

public interface BaseRepository<E, ID> extends ReactiveStreamsCrudRepository<E, ID> {

    @Override
    @NonNull
    @SingleResult
    Mono<E> findById(@NotNull @NonNull ID id);

}
