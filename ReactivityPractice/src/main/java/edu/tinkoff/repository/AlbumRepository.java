package edu.tinkoff.repository;

import edu.tinkoff.model.Album;
import io.micronaut.data.annotation.Join;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.r2dbc.annotation.R2dbcRepository;
import reactor.core.publisher.Mono;

@R2dbcRepository(dialect = Dialect.POSTGRES)
public interface AlbumRepository extends BaseRepository<Album, Long> {

    @Join(value = "artist", type = Join.Type.LEFT_FETCH)
    Mono<Album> queryById(Long id);

}
