package edu.tinkoff.repository;

import edu.tinkoff.model.Artist;
import io.micronaut.data.annotation.Join;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.r2dbc.annotation.R2dbcRepository;
import reactor.core.publisher.Mono;

@R2dbcRepository(dialect = Dialect.POSTGRES)
public interface ArtistRepository extends BaseRepository<Artist, Long> {

    @Join(value = "albums", type = Join.Type.LEFT_FETCH)
    Mono<Artist> queryById(Long id);

}
