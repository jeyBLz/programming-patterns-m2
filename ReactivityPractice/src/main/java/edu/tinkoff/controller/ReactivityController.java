package edu.tinkoff.controller;

import edu.tinkoff.model.Album;
import edu.tinkoff.model.Artist;
import edu.tinkoff.repository.AlbumRepository;
import edu.tinkoff.repository.ArtistRepository;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import jakarta.inject.Inject;
import reactor.core.publisher.Mono;

import java.util.List;

@Controller("/")
public class ReactivityController {

    @Inject
    private ArtistRepository artistRepository;
    @Inject
    private AlbumRepository albumRepository;

    @Get("/artist/{id}")
    public Mono<Album> getArtistById(@PathVariable(name = "id") long id) {
        return albumRepository.findById(id);
    }

    @Get("/album/{id}")
    public Mono<Album> getAlbumById(@PathVariable(name = "id") long id) {
        return albumRepository.queryById(id);
    }

}
