import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) throws IOException {
        try (ServerSocket server = new ServerSocket(3000)) {
            while (true) {
                try (Socket client = server.accept()) {
                    DataInputStream dis = new DataInputStream(client.getInputStream());
                    DataOutputStream dos = new DataOutputStream(client.getOutputStream());

                    double first = dis.readDouble();
                    double second = dis.readDouble();

                    dos.writeDouble(first * second);
                }
            }
        }
    }

}
