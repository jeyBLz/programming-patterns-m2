package edu.ssau;

import javax.swing.*;

public class MyFrame extends JFrame {

    private final JPanel panel;

    public MyFrame() {
        panel = new MyPanel();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(panel);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
    }

}
