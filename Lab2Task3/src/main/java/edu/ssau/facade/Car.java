package edu.ssau.facade;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class Car {

    private int x = 0;
    private final int y = 250;

    private int velocity = 8;

    private Image image;

    private final JPanel panel;

    public Car(JPanel panel) {
        image = new ImageIcon("src/main/resources/car-2.png").getImage();
        new Timer(10, this::actionPerformed).start();
        this.panel = panel;
    }

    public int getX() {
        return x;
    }

    public void setVelocity(int velocity) {
        this.velocity = velocity;
    }

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(image, x, y, null);
    }

    public void actionPerformed(ActionEvent e) {
        x += velocity;
        if (x >= panel.getWidth()) {
            x = -image.getWidth(null);
        }
        panel.repaint();
    }
}
