package edu.ssau.facade;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class TrafficFacade {

    private final Car car;
    private final TrafficLight light;

    public TrafficFacade(JPanel panel) {
        this.car = new Car(panel);
        this.light = new TrafficLight(panel);
        new Timer(10, this::actionPerformed).start();
    }

    public void paint(Graphics g) {
        car.paint(g);
        light.paint(g);
    }

    public void actionPerformed(ActionEvent event) {
        if (light.isGreen() || car.getX() <= 150 || car.getX() > 200) {
            car.setVelocity(10);
            return;
        }
        if (light.isRed()) {
            car.setVelocity(0);
        }
    }

}
