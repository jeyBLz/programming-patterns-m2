package edu.ssau.facade;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class TrafficLight {

    private final JPanel panel;
    private Light light;
    private int lightStep = 1;

    private final int X = 250;
    private final int Y = 200;

    private final int SIZE = 15;

    public TrafficLight(JPanel panel) {
        light = Light.GREEN;
        new Timer(1000, this::actionPerformed).start();
        this.panel = panel;
    }

    private enum Light {
        GREEN(Color.GREEN),
        YELLOW(Color.YELLOW),
        RED(Color.RED)
        ;

        private final Color color;

        Light(Color color) {
            this.color = color;
        }
    }

    public void next() {
        if (light == Light.GREEN) {
            lightStep = 1;
        } else if (light == Light.RED) {
            lightStep = -1;
        }
        light = Light.values()[light.ordinal() + lightStep];
    }

    public boolean isGreen() {
        return light == Light.GREEN;
    }

    public boolean isNextGreen() {
        return Light.values()[light.ordinal() + lightStep] == Light.GREEN;
    }

    public boolean isYellow() {
        return light == Light.YELLOW;
    }

    public boolean isRed() {
        return light == Light.RED;
    }

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setPaint(light.color);
        g2.fillOval(X, Y, SIZE, SIZE);
    }

    public void actionPerformed(ActionEvent event) {
        next();
        panel.repaint();
    }

}
