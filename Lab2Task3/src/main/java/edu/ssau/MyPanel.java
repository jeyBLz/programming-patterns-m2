package edu.ssau;

import edu.ssau.facade.TrafficFacade;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {

    private final int WIDTH = 500;
    private final int HEIGHT = 500;

    private final TrafficFacade facade;

    public MyPanel() {
        facade = new TrafficFacade(this);

        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setBackground(Color.BLACK);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        facade.paint(g);
    }
}
