package edu.ssau;

import edu.ssau.controller.Controller;
import edu.ssau.model.Model;
import edu.ssau.view.View;

public class Main {

    public static void main(String[] args) {
        Model model = new Model();
        View view = new View();
        Controller controller = new Controller(model, view);
    }

}
