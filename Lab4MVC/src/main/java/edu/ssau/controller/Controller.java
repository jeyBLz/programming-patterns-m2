package edu.ssau.controller;

import edu.ssau.model.Model;
import edu.ssau.model.Point;
import edu.ssau.view.View;

import javax.swing.*;
import java.util.List;

public class Controller {

    private final Model model;
    private final View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;

        view.getAddButton().addActionListener((e) -> {
            String xString = JOptionPane.showInputDialog(view, "Enter X value:");
            if (xString != null && xString.length() > 0) {
                try {
                    double x = Double.parseDouble(xString);
                    model.add(x);
                    updateTableAndGraph();
                } catch(NumberFormatException error) {
                    JOptionPane.showMessageDialog(view, "The input value is not double",
                            "Error add", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        view.getEditButton().addActionListener((e) -> {
            int selectedRow = view.getTable().getSelectedRow();
            if (selectedRow != -1) {
                String xString = JOptionPane.showInputDialog(view, "Enter new X value:");
                if (xString != null && xString.length() > 0) {
                    try {
                        double x = Double.parseDouble(xString);
                        model.edit(selectedRow, x);
                        updateTableAndGraph();
                    } catch (NumberFormatException error) {
                        JOptionPane.showMessageDialog(view, "The input value is not double",
                                "Error edit", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        view.getDeleteButton().addActionListener((e) -> {
            int[] selectedRows = view.getTable().getSelectedRows();
            for (int i = selectedRows.length - 1; i >= 0; i--) {
                model.remove(selectedRows[i]);
            }
            updateTableAndGraph();
        });
    }

    private void updateTableAndGraph() {
        List<Point> points = model.getPoints();
        view.updateTable(points);
    }

}
