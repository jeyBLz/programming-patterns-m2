package edu.ssau.view;

import edu.ssau.model.Point;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class View extends JFrame {
    private JPanel tablePanel;
    private JTable table;
    private JButton addButton;
    private JButton editButton;
    private JButton deleteButton;
    private JPanel graphPanel;
    private JPanel graphComponent;
    private XYSeries series;

    public View() {
        setTitle("Super Mega Chart");
        setSize(800, 600);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        tablePanel = new JPanel();
        tablePanel.setLayout(new BorderLayout());

        DefaultTableModel defaultTableModel = new DefaultTableModel(new Object[][]{}, new String[] {"X", "Y"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        table = new JTable(defaultTableModel);
        JScrollPane scrollPane = new JScrollPane(table);
        tablePanel.add(scrollPane, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1, 3));

        addButton = new JButton("Add");
        buttonPanel.add(addButton);

        editButton = new JButton("Edit");
        buttonPanel.add(editButton);

        deleteButton = new JButton("Remove");
        buttonPanel.add(deleteButton);

        tablePanel.add(buttonPanel, BorderLayout.SOUTH);

        graphPanel = new JPanel();
        graphPanel.setLayout(new BorderLayout());
        graphComponent = createChartPanel();
        graphPanel.add(graphComponent, BorderLayout.CENTER);

        setLayout(new GridLayout(1, 2));
        add(graphPanel);
        add(tablePanel);

        setVisible(true);
    }

    public void updateTable(List<Point> points) {
        int rowCount = table.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            ((DefaultTableModel) table.getModel()).removeRow(i);
        }
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        series.delete(0, series.getItemCount() - 1);
        points.forEach(point -> {
            model.addRow(new Object[] {point.getX(), point.getY()});
            series.add(point.getX(), point.getY());
        });
    }

    public JPanel createChartPanel() {
        String chartTitle = "Chart";
        String xAxisLabel = "X";
        String yAxisLabel = "Y";

        XYDataset dataset = createDataset();

        JFreeChart chart = ChartFactory.createXYLineChart(chartTitle, xAxisLabel, yAxisLabel, dataset);

        return new ChartPanel(chart);
    }

    private XYDataset createDataset() {
        XYSeriesCollection dataset = new XYSeriesCollection();
        series = new XYSeries("sin(x)^2 + cos(x)^2");

        dataset.addSeries(series);

        return dataset;
    }

    public JButton getAddButton() {
        return addButton;
    }

    public JButton getEditButton() {
        return editButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public JTable getTable() {
        return table;
    }
}
