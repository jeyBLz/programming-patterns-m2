package edu.ssau.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Model {

    private final List<Point> points;

    public Model() {
        this.points = new ArrayList<>();
    }

    public void add(double x) {
        double y = calculateFromX(x);
        Point point = new Point(x, y);
        points.add(point);
        points.sort(Comparator.comparingDouble(Point::getX));
    }

    public void edit(int index, double x) {
        Point point = points.get(index);
        point.setX(x);
        point.setY(calculateFromX(x));
    }

    public void remove(int index) {
        points.remove(index);
    }

    public List<Point> getPoints() {
        return points;
    }

    private double calculateFromX(double x) {
        return Math.ceil(Math.pow(Math.sin(x), 2) + Math.pow(Math.cos(x), 2)) - 1;
    }

}
