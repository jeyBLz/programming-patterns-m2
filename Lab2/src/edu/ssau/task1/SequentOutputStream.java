package edu.ssau.task1;

import java.io.IOException;
import java.io.OutputStream;

public class SequentOutputStream {

    private final OutputStream os;

    public SequentOutputStream(OutputStream os) {
        this.os = os;
    }

    public void write(String[] strings) throws IOException {
        for (String string : strings) {
            os.write(string.getBytes());
            os.write('\n');
        }
    }

}
