package edu.ssau.task4;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public final class Proxy {

    private Proxy() {}

    public static double product(double first, double second) throws IOException {
        try (Socket socket = new Socket("localhost", 3000)) {
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            DataInputStream dis = new DataInputStream(socket.getInputStream());

            dos.writeDouble(first);
            dos.writeDouble(second);

            return dis.readDouble();
        }
    }

}
