package edu.ssau.task2.exception;

public class ModelPriceOutOfBoundsException extends RuntimeException {

    public ModelPriceOutOfBoundsException() {}

    public ModelPriceOutOfBoundsException(String message) {
        super(message);
    }

}
