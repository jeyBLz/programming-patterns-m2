package edu.ssau.task2.exception;

public class NoSuchModelNameException extends Exception {

    public NoSuchModelNameException() {}

    public NoSuchModelNameException(String message) {
        super(message);
    }

}
