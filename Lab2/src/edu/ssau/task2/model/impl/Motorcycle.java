package edu.ssau.task2.model.impl;

import edu.ssau.task2.exception.DuplicateModelNameException;
import edu.ssau.task2.exception.ModelPriceOutOfBoundsException;
import edu.ssau.task2.exception.NoSuchModelNameException;
import edu.ssau.task2.model.Vehicle;

import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

public class Motorcycle implements Vehicle {

    private class Model implements Cloneable {
        String name;
        double price;
        Model next;
        Model prev;

        public Model() {}

        public Model(String name, double price) {
            this.name = name;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        @Override
        protected Model clone() throws CloneNotSupportedException {
            return (Model) super.clone();
        }
    }

    private String name;
    private Model head = new Model();
    private int size;

    {
        head.next = head;
        head.prev = head;
    }

    public Motorcycle(String name, int size) {
        this.name = name;
        this.size = size;
        Random random = new Random();
        Stream.iterate(0, i -> i < size, i -> i + 1)
                .forEach(i -> {
                    this.addWithNoExceptions("Motorcycle #" + (i + 1), (random.nextInt(5000) + 5000) / 100.);
                });
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public double getPriceByModelName(String modelName) throws NoSuchModelNameException {
        return getModelByName(modelName).getPrice();
    }

    @Override
    public void setPriceByModelName(String modelName, double price) throws NoSuchModelNameException {
        if (price < 0.) {
            throw new ModelPriceOutOfBoundsException();
        }
        getModelByName(modelName).setPrice(price);
    }

    @Override
    public String[] getModelNames() {
        final Model[] temp = {head};
        return Stream.iterate(0, i -> i < size, i -> i + 1)
                .map(i -> {
                    temp[0] = temp[0].next;
                    return temp[0].getName();
                })
                .toArray(String[]::new);
    }

    @Override
    public double[] getPrices() {
        final Model[] temp = {head};
        return Stream.iterate(0, i -> i < size, i -> i + 1)
                .mapToDouble(i -> {
                    temp[0] = temp[0].next;
                    return temp[0].getPrice();
                })
                .toArray();
    }

    @Override
    public void add(String modelName, double price) throws DuplicateModelNameException {
        if (price < 0.) {
            throw new ModelPriceOutOfBoundsException();
        }
        if (findModelByName(modelName).isPresent()) {
            throw new DuplicateModelNameException(String.format("Model with name %s already exists", modelName));
        }
        this.addWithNoExceptions(modelName, price);
        size++;
    }

    @Override
    public void remove(String modelName) throws NoSuchModelNameException {
        Model model = getModelByName(modelName);
        model.prev.next = model.next;
        model.next.prev = model.prev;
        size--;
    }

    @Override
    public int getSize() {
        return size;
    }

    private Optional<Model> findModelByName(String modelName) {
        final Model[] temp = {head};
        return Stream.iterate(0, i -> i < size, i -> i + 1)
                .map(i -> {
                    temp[0] = temp[0].next;
                    if (temp[0].getName().equals(modelName)) {
                        return temp[0];
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .findFirst();
    }

    private Model getModelByName(String modelName) throws NoSuchModelNameException {
        return findModelByName(modelName)
                .orElseThrow(() -> new NoSuchModelNameException("There is no model with name " + modelName));
    }

    private void addWithNoExceptions(String modelName, double price) {
        Model model = new Model(modelName, price);
        model.prev = head.prev;
        model.next = head;
        head.prev.next = model;
        head.prev = model;
    }

    private void addWithNoExceptions(Model model) {
        model.next = head;
        model.prev = head.prev;

        head.prev.next = model;
        head.prev = model;
    }

    @Override
    public Motorcycle clone() throws CloneNotSupportedException {
        Motorcycle clone = (Motorcycle) super.clone();
        clone.head = head.clone();
        final Model[] temp = {head};
        clone.head.next = clone.head;
        clone.head.prev = clone.head;
        Stream.iterate(0, i -> i < size, i -> i + 1)
                .forEach(i -> {
                    temp[0] = temp[0].next;
                    try {
                        clone.addWithNoExceptions(temp[0].clone());
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                });
        return clone;
    }
}
