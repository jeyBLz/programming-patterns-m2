package edu.ssau.task2.model.impl;

import edu.ssau.task2.exception.DuplicateModelNameException;
import edu.ssau.task2.exception.ModelPriceOutOfBoundsException;
import edu.ssau.task2.exception.NoSuchModelNameException;
import edu.ssau.task2.model.Vehicle;

import java.util.Arrays;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

public class Car implements Vehicle {

    private String name;
    private Model[] models;

    public Car(String name, int size) {
        this.name = name;
        models = new Model[size];

        Random random = new Random();
        Stream.iterate(0, i -> i + 1)
                .limit(size)
                .forEach(i ->
                        models[i] = new Model("Car #" + (i + 1), (random.nextInt(5000) + 5000) / 100.)
                );
    }

    private class Model implements Cloneable {
        private String name;
        private double price;

        public Model(String name, double price) {
            this.name = name;
            this.price = price;
        }

        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }

        private double getPrice() {
            return price;
        }

        private void setPrice(double price) {
            this.price = price;
        }

        @Override
        protected Model clone() throws CloneNotSupportedException {
            return (Model) super.clone();
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public double getPriceByModelName(String modelName) throws NoSuchModelNameException {
        return getModelByName(modelName).getPrice();
    }

    @Override
    public void setPriceByModelName(String modelName, double price) throws NoSuchModelNameException {
        if (price < 0.) {
            throw new ModelPriceOutOfBoundsException();
        }
        getModelByName(modelName).setPrice(price);
    }

    @Override
    public String[] getModelNames() {
        return Arrays.stream(models)
                .map(Model::getName)
                .toArray(String[]::new);
    }

    @Override
    public double[] getPrices() {
        return Arrays.stream(models)
                .mapToDouble(Model::getPrice)
                .toArray();
    }

    @Override
    public void add(String modelName, double price) throws DuplicateModelNameException {
        if (price < 0.) {
            throw new ModelPriceOutOfBoundsException();
        }
        if (findModelByName(modelName).isPresent()) {
            throw new DuplicateModelNameException(String.format("Model with name %s already exists", modelName));
        }
        Model model = new Model(modelName, price);
        models = Arrays.copyOf(models, models.length + 1);
        models[models.length - 1] = model;
    }

    @Override
    public void remove(String modelName) throws NoSuchModelNameException {
        int index = Stream.iterate(0, i -> i + 1)
                .limit(models.length)
                .filter(i -> models[i].getName().equals(modelName))
                .findFirst()
                .orElseThrow(() -> new NoSuchModelNameException("There is no model with name " + modelName));
        System.arraycopy(models, index + 1, models, index, models.length - index - 1);
        models = Arrays.copyOf(models, models.length - 1);
    }

    @Override
    public int getSize() {
        return models.length;
    }

    private Optional<Model> findModelByName(String modelName) {
        return Arrays.stream(models)
                .filter(m -> m.getName().equals(modelName))
                .findFirst();
    }

    private Model getModelByName(String modelName) throws NoSuchModelNameException {
        return findModelByName(modelName)
                .orElseThrow(() -> new NoSuchModelNameException("There is no model with name " + modelName));
    }

    @Override
    public Car clone() throws CloneNotSupportedException {
        Car clone = (Car) super.clone();
        clone.models = Arrays.stream(models).map(model -> {
            try {
                return model.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return null;
        }).toArray(Model[]::new);
        return clone;
    }
}
