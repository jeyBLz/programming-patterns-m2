package edu.ssau.task2.factory;

import edu.ssau.task2.model.Vehicle;
import edu.ssau.task2.model.impl.Motorcycle;

public class MotorcycleFactory implements VehicleFactory {
    @Override
    public Vehicle createInstance(String name, int size) {
        return new Motorcycle(name, size);
    }
}
