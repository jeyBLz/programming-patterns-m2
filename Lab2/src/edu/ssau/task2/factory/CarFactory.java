package edu.ssau.task2.factory;

import edu.ssau.task2.model.Vehicle;
import edu.ssau.task2.model.impl.Car;

public class CarFactory implements VehicleFactory {
    @Override
    public Vehicle createInstance(String name, int size) {
        return new Car(name, size);
    }
}
