package edu.ssau;

import edu.ssau.task1.SequentOutputStream;
import edu.ssau.task2.VehicleUtils;
import edu.ssau.task2.model.Vehicle;
import edu.ssau.task2.model.impl.Car;

import java.io.IOException;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws IOException {
        SequentOutputStream sos = new SequentOutputStream(System.out);
        sos.write(new String[] {"1", "2"});

        Vehicle vehicle = VehicleUtils.synchronizedVehicle(new Car("Mercedes", 10));
        System.out.println(Arrays.toString(vehicle.getModelNames()));
    }

}
