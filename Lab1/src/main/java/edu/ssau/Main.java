package edu.ssau;

import edu.ssau.task1.PropertiesReader;
import edu.ssau.task2.VehicleUtils;
import edu.ssau.task2.exception.DuplicateModelNameException;
import edu.ssau.task2.exception.NoSuchModelNameException;
import edu.ssau.task2.factory.MotorcycleFactory;
import edu.ssau.task2.model.Vehicle;
import edu.ssau.task2.model.impl.Car;
import edu.ssau.task2.model.impl.Motorcycle;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws CloneNotSupportedException {
//        task1();
        task2();
    }

    public static void task1() {
        PropertiesReader pr = PropertiesReader.getInstance();
        System.out.println(pr.getProperties());

        PropertiesReader pr2 = PropertiesReader.getInstance();
        System.out.println(pr == pr2);
    }

    public static void task2() throws CloneNotSupportedException {
        Vehicle car = VehicleUtils.createInstance("BMW", 10);
        VehicleUtils.printModelNames(car);
        VehicleUtils.printModelPrices(car);
        System.out.println(VehicleUtils.getAveragePrice(car));

        try {
            car.add("Car #1", 10);
        } catch (DuplicateModelNameException e) {
            e.printStackTrace();
        }

        try {
            car.remove("Car #11");
        } catch (NoSuchModelNameException e) {
            e.printStackTrace();
        }

        try {
            car.add("Car #11", 10);
            System.out.println(Arrays.toString(car.getModelNames()));
            System.out.println(Arrays.toString(car.getPrices()));
            car.remove("Car #11");
            System.out.println(Arrays.toString(car.getModelNames()));
            System.out.println(Arrays.toString(car.getPrices()));
        } catch (Exception ignore) {}

        VehicleUtils.setFactory(new MotorcycleFactory());

        Vehicle motorcycle = VehicleUtils.createInstance("Yamaha", 10);
        VehicleUtils.printModelNames(motorcycle);
        VehicleUtils.printModelPrices(motorcycle);
        System.out.println(VehicleUtils.getAveragePrice(motorcycle));

        try {
            motorcycle.add("Motorcycle #1", 10);
        } catch (DuplicateModelNameException e) {
            e.printStackTrace();
        }

        try {
            motorcycle.remove("Motorcycle #11");
        } catch (NoSuchModelNameException e) {
            e.printStackTrace();
        }

        try {
            motorcycle.add("Motorcycle #11", 10);
            System.out.println(Arrays.toString(motorcycle.getModelNames()));
            System.out.println(Arrays.toString(motorcycle.getPrices()));
            motorcycle.remove("Motorcycle #11");
            System.out.println(Arrays.toString(motorcycle.getModelNames()));
            System.out.println(Arrays.toString(motorcycle.getPrices()));
        } catch (Exception ignore) {}

        Vehicle clone = motorcycle.clone();
        System.out.println(motorcycle);
        System.out.println(clone);
    }

}