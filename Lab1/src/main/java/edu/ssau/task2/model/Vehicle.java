package edu.ssau.task2.model;

import edu.ssau.task2.exception.DuplicateModelNameException;
import edu.ssau.task2.exception.NoSuchModelNameException;

public interface Vehicle extends Cloneable {

    String getName();
    void setName(String name);
    double getPriceByModelName(String modelName) throws NoSuchModelNameException;
    void setPriceByModelName(String modelName, double price) throws NoSuchModelNameException;
    String[] getModelNames();
    double[] getPrices();
    void add(String modelName, double price) throws DuplicateModelNameException;
    void remove(String modelName) throws NoSuchModelNameException;
    int getSize();
    Vehicle clone() throws CloneNotSupportedException;

}
