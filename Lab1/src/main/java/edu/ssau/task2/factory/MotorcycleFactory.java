package edu.ssau.task2.factory;

import edu.ssau.task2.model.impl.Motorcycle;
import edu.ssau.task2.model.Vehicle;

public class MotorcycleFactory implements VehicleFactory {
    @Override
    public Vehicle createInstance(String name, int size) {
        return new Motorcycle(name, size);
    }
}
