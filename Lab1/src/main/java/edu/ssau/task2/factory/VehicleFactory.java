package edu.ssau.task2.factory;

import edu.ssau.task2.model.Vehicle;

public interface VehicleFactory {

    Vehicle createInstance(String name, int size);

}
