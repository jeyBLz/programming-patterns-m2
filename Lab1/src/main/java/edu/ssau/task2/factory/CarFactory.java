package edu.ssau.task2.factory;

import edu.ssau.task2.model.impl.Car;
import edu.ssau.task2.model.Vehicle;

public class CarFactory implements VehicleFactory {
    @Override
    public Vehicle createInstance(String name, int size) {
        return new Car(name, size);
    }
}
