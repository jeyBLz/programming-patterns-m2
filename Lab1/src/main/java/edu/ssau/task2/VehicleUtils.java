package edu.ssau.task2;

import edu.ssau.task2.factory.CarFactory;
import edu.ssau.task2.factory.VehicleFactory;
import edu.ssau.task2.model.Vehicle;

import java.util.Arrays;

public final class VehicleUtils {

    private static VehicleFactory FACTORY = new CarFactory();

    public static void setFactory(VehicleFactory factory) {
        VehicleUtils.FACTORY = factory;
    }

    private VehicleUtils() {}

    public static double getAveragePrice(Vehicle vehicle) {
        return Arrays.stream(vehicle.getPrices()).average().orElse(0.);
    }

    public static void printModelNames(Vehicle vehicle) {
        Arrays.stream(vehicle.getModelNames()).forEach(System.out::println);
    }

    public static void printModelPrices(Vehicle vehicle) {
        Arrays.stream(vehicle.getPrices()).forEach(System.out::println);
    }

    public static Vehicle createInstance(String name, int size) {
        return FACTORY.createInstance(name, size);
    }

}
