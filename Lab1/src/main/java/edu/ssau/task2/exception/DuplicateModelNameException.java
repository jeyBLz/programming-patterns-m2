package edu.ssau.task2.exception;

public class DuplicateModelNameException extends Exception {

    public DuplicateModelNameException() {}

    public DuplicateModelNameException(String message) {
        super(message);
    }

}
