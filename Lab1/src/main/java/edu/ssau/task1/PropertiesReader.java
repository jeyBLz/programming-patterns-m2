package edu.ssau.task1;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {

    private static final String RESOURCE_LOCATION =
            PropertiesReader.class.getProtectionDomain().getCodeSource().getLocation().toString().substring("file:\\".length());

    private static PropertiesReader INSTANCE;
    private static Properties PROPERTIES;

    private PropertiesReader() {}

    public static PropertiesReader getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PropertiesReader();
        }
        return INSTANCE;
    }

    public Properties getProperties() {
        if (PROPERTIES != null) {
            return PROPERTIES;
        }
        try (FileInputStream fis = new FileInputStream(RESOURCE_LOCATION + "config.properties")) {
            PROPERTIES = new Properties();
            PROPERTIES.load(fis);
            return PROPERTIES;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
